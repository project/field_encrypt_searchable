<?php

namespace Drupal\field_encrypt_searchable;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;


/**
 * Interface for service class to process entities and fields for encryption.
 */
interface FieldEncryptSearchableProcessEntitiesInterface {

  /**
   * Check if entity has blind index fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to check.
   *
   * @return array
   *   Return array of name of blind index fields.
   */
  public function entityHasBlindIndexFields(ContentEntityInterface $entity): array;

  /**
   * Blind index an entity.
   *
   * @param ContentEntityInterface $entity
   */
  public function processBlindIndex(ContentEntityInterface $entity);

  /**
   * Blind index an entity without check if value changed.
   *
   * @param ContentEntityInterface $entity
   */
  public function reProcessBlindIndex(ContentEntityInterface $entity);

  /**
   * Delete blind index when delete an entity.
   *
   * @param ContentEntityInterface $entity
   */
  public function deleteBlindIndex(ContentEntityInterface $entity);

  /**
   * Return the encryption key.
   */
  public function getEncryptionKey();
}

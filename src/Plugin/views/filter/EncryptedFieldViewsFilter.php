<?php

namespace Drupal\field_encrypt_searchable\Plugin\views\filter;

/**
 * Encrypted field views filter.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("encrypted_field_filter")
 */

use Drupal;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_encrypt_searchable\Transformation\SearchLikeTransformation;
use Drupal\views\Plugin\views\filter\StringFilter;
use Drupal\views\Views;
use ParagonIE\CipherSweet\Backend\FIPSCrypto;
use ParagonIE\CipherSweet\BlindIndex;
use ParagonIE\CipherSweet\CipherSweet;
use ParagonIE\CipherSweet\EncryptedField;
use ParagonIE\CipherSweet\KeyProvider\StringProvider;

/**
 * Provides a custom filter for encrypted fields.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsFilter("encrypted_field_filter")
 */
class EncryptedFieldViewsFilter extends StringFilter {

  /**
   * {@inheritDoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['fields'] = ['default' => ''];
    $options['field_property'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $this->view->initStyle();

    // Allow to choose all fields as possible
    if ($this->view->style_plugin->usesFields()) {
      $options = $fieldProperties = [];
      foreach ($this->view->display_handler->getHandlers('field') as $name => $field) {
        $definition = $field->definition;
        /** @var EntityFieldManagerInterface $entityFieldManager */
        $entityFieldManager = \Drupal::service('entity_field.manager');
        // if ($name == 'field_ckeditor_1') {
        //   dd($definition);
        // }
        $storage = $entityFieldManager->getFieldStorageDefinitions($definition['entity_type'])[$definition['field_name']];
        if (empty($storage) || !is_callable([$storage, 'getThirdPartySetting'])) {
          continue;
        }
        // Check if the field is encrypted.
        $hasBlindIndex = $storage->getThirdPartySetting('field_encrypt', 'encrypt', FALSE)
          && $storage->getThirdPartySetting('field_encrypt', 'blind_index', FALSE);
        $blindIndexProperties = [] ;
        foreach ($storage->getThirdPartySetting('field_encrypt', 'properties', []) as $property) {
          if (!empty($property)) {
            $blindIndexProperties[$property] = $property;
          }
        }
        if ($hasBlindIndex && !empty($blindIndexProperties)) {
          $options[$name] = $field->adminLabel(TRUE);
          $fieldProperties[$name] = $blindIndexProperties;
        }
      }
      // dd($options);
      $form['operator']['#options'] = array_intersect_key($form['operator']['#options'], array_flip(['contains']));
      if (!empty($options)) {
        $form['fields'] = [
          '#type' => 'select',
          '#title' => $this->t('Choose encrypted field for filtering'),
          '#description' => $this->t("This filter doesn't work for very special field handlers."),
          '#options' => $options,
          '#default_value' => $this->options['fields'],
          '#ajax' => [
            'callback' => '\Drupal\field_encrypt_searchable\Plugin\views\filter\EncryptedFieldViewsFilter::fieldsAjaxCallBack',
            'wrapper' => 'edit-field-property'
          ]
        ];
        $selectedField = isset($form_state->getUserInput()['options']['fields'])
          ? $form_state->getUserInput()['options']['fields']
          : $this->options['fields'];
        if (empty($selectedField) && !empty($fieldProperties)) {
          $fieldPropertyKeys = array_keys($fieldProperties);
          $selectedField = reset($fieldPropertyKeys);
        }
        $form['field_property'] = [
          '#type' => 'select',
          '#title' => $this->t('Field property'),
          '#options' => $fieldProperties[$selectedField],
          '#default_value' => $this->options['field_property'],
          '#multiple' => TRUE,
          '#prefix' => '<div id="edit-field-property">',
          '#suffix' => '</div>',
        ];
      }
    }
  }

  /**
   * Change properties option when change field to filter.
   */
  public function fieldsAjaxCallBack(&$form, FormStateInterface $form_state) {
    return $form['options']['field_property'];
  }

  /**
   * {@inheritDoc}
   */
  public function query()
  {
    if (!empty($this->options['fields'])
      && !empty($this->options['field_property'])) {
        // dd($this);
      $selectedFieldId = $this->options['fields'];
      $field = $this->view->field[$selectedFieldId];

      /** @var \Drupal\field_encrypt_searchable\FieldEncryptSearchableProcessEntitiesInterface $encryptionSearchableService */
      $encryptionSearchableService = Drupal::service('field_encrypt_searchable.process_entities');
      $encryptionKey = $encryptionSearchableService->getEncryptionKey();

      $provider = new StringProvider($encryptionKey);
      $engine = new CipherSweet($provider, new FIPSCrypto());
      $prepareBlindIndex = (new EncryptedField($engine));
      $prepareBlindIndex->addBlindIndex(
        new BlindIndex(
          "encrypt_searchable",
          [new SearchLikeTransformation(0, mb_strlen($this->value))],
          128,
          TRUE
        )
      );
      $index = $prepareBlindIndex->getBlindIndex($this->value, 'encrypt_searchable');

      $field->ensureMyTable();

      $extra = [
        [
          'field' => 'langcode',
          'value' => Drupal::languageManager()->getCurrentLanguage()->getId(),
        ],
        [
          'field' => 'status',
          'value' => 1,
        ],
      ];

      if (count($this->options['field_property']) > 1) {
        $extra[] = [
          'field' => 'entity_field_property',
          'value' => $this->options['field_property'],
          'operator' => 'IN',
        ];
      }
      elseif (count($this->options['field_property']) === 1) {
        $extra[] = [
          'field' => 'entity_field_property',
          'value' => reset($this->options['field_property']),
          'operator' => '=',
        ];
      }

      $tableName = "blind_index__{$field->definition['entity_type']}__{$field->field}";

      $configuration = [
        'table' => $tableName,
        'field' => 'entity_id',
        'left_table' => $field->tableAlias,
        'left_field' => 'entity_id',
        'operator' => '=',
        'extra' => $extra,
      ];
      $join = Views::pluginManager('join')
        ->createInstance('standard', $configuration);
      $this->query->addRelationship("{$field->tableAlias}__{$tableName}", $join, $field->tableAlias);
      $this->query->addWhere('AND', "{$field->tableAlias}__{$tableName}.index_value", $index);
      $this->query->addField("{$field->tableAlias}__{$tableName}", 'entity_id');
    }
  }

}

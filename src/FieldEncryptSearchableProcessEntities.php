<?php

namespace Drupal\field_encrypt_searchable;

use Drupal;
use Drupal\Component\Utility\DiffArray;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldConfigStorageBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\field_encrypt_searchable\Transformation\SearchLikeTransformation;
use ParagonIE\CipherSweet\Backend\FIPSCrypto;
use ParagonIE\CipherSweet\BlindIndex;
use ParagonIE\CipherSweet\CipherSweet;
use ParagonIE\CipherSweet\EncryptedField;
use ParagonIE\CipherSweet\KeyProvider\StringProvider;
use React\EventLoop\Loop;
use React\Promise\Deferred;
use function GuzzleHttp\Promise\all;

/**
 * Service class to process entities and fields for blind index.
 */
class FieldEncryptSearchableProcessEntities implements FieldEncryptSearchableProcessEntitiesInterface {

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $databaseConnection;

  /**
   * FieldEncryptSearchableProcessEntities constructor.
   *
   * @param \Drupal\Core\Database\Connection $databaseConnection
   */
  public function __construct(Connection $databaseConnection) {
    $this->databaseConnection = $databaseConnection;
  }


  /**
   * {@inheritdoc}
   */
  public function entityHasBlindIndexFields(ContentEntityInterface $entity): array {
    // Make sure we can get fields.
    if (!is_callable([$entity, 'getFields'])) {
      return [];
    }

    $blindIndexFields = [];
    foreach ($entity->getFields() as $field) {
      if (!empty($blindIndexProperties = $this->checkBlindIndexProperties($field))) {
        $blindIndexFields[$field->getName()] = $blindIndexProperties;
      }
    }

    return $blindIndexFields;
  }

  /**
   * Check if a given field has encryption enabled.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field to check.
   *
   * @return array
   *   Array of blind index properties.
   */
  protected function checkBlindIndexProperties(FieldItemListInterface $field): array {
    if (!is_callable([$field, 'getFieldDefinition'])) {
      return [];
    }

    /* @var $definition BaseFieldDefinition */
    $definition = $field->getFieldDefinition();

    if (!is_callable([$definition, 'get'])) {
      return [];
    }

    /* @var $storage FieldConfigStorageBase */
    $storage = $definition->get('fieldStorage');
    if (is_null($storage)) {
      return [];
    }

    // Check if the field is encrypted.
    $hasBlindIndex = $storage->getThirdPartySetting('field_encrypt', 'encrypt', FALSE) && $storage->getThirdPartySetting('field_encrypt', 'blind_index', FALSE);
    if ($hasBlindIndex) {
      $properties = $storage->getThirdPartySetting('field_encrypt', 'properties', []);
      $blindIndexProperties = [];
      foreach ($properties as $property) {
        if (!empty($property)) {
          $blindIndexProperties[] = $property;
        }
      }
      return $blindIndexProperties;
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function processBlindIndex(ContentEntityInterface $entity) {
    Drupal::service('field_encrypt.process_entities')->decryptEntity($entity);

    $blindIndexFields = $this->entityHasBlindIndexFields($entity);
    if (!empty($blindIndexFields)) {
      $encryption_key = $this->getEncryptionKey();
      $provider = new StringProvider($encryption_key);
      $engine = new CipherSweet($provider, new FIPSCrypto());

      /** @var ContentEntityInterface $original */
      $original = !empty($entity->original) ? $entity->original : NULL;
      if (!empty($original)) {
        Drupal::service('field_encrypt.process_entities')
          ->decryptEntity($original);
      }

      foreach ($blindIndexFields as $fieldName => $blindIndexProperties) {
        if ($entity->hasField($fieldName) && !empty($blindIndexProperties)) {
          $values = $entity->{$fieldName}->getValue();

          if (!empty($original) && $original->hasField($fieldName)) {
            $originalValues = $original->{$fieldName}->getValue();
            if (count($originalValues) === count($values)
              && empty(DiffArray::diffAssocRecursive($values, $originalValues))
              && empty(DiffArray::diffAssocRecursive($originalValues, $values))) {
              continue;
            }
          }

          foreach ($values as $delta => $properties) {
            foreach ($properties as $property => $value) {
              if (in_array($property, $blindIndexProperties)) {
                $this->createBlindIndexesForValueReact($engine, $value, $entity, $fieldName, $delta, $property);
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function reProcessBlindIndex(ContentEntityInterface $entity)
  {
    Drupal::service('field_encrypt.process_entities')->decryptEntity($entity);

    $blindIndexFields = $this->entityHasBlindIndexFields($entity);
    if (!empty($blindIndexFields)) {
      $encryption_key = $this->getEncryptionKey();
      $provider = new StringProvider($encryption_key);
      $engine = new CipherSweet($provider, new FIPSCrypto());

      foreach ($blindIndexFields as $fieldName => $blindIndexProperties) {
        if ($entity->hasField($fieldName) && !empty($blindIndexProperties)) {
          $values = $entity->{$fieldName}->getValue();

          foreach ($values as $delta => $properties) {
            foreach ($properties as $property => $value) {
              if (in_array($property, $blindIndexProperties)) {
                $this->createBlindIndexesForValueReact($engine, $value, $entity, $fieldName, $delta, $property);
              }
            }
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteBlindIndex(ContentEntityInterface $entity) {
    Drupal::service('field_encrypt.process_entities')->decryptEntity($entity);
    $blindIndexFields = $this->entityHasBlindIndexFields($entity);
    if (!empty($blindIndexFields)) {
      foreach (array_keys($blindIndexFields) as $blindIndexField) {
        if ($entity->hasField($blindIndexField)) {
          $query = [
            'status' => TRUE,
            'entity_id' => $entity->id(),
            'entity_bundle' => $entity->bundle(),
          ];

          $tableName = "blind_index__{$entity->getEntityTypeId()}__{$blindIndexField}";

          $deleteQuery = $this->databaseConnection->delete($tableName);
          foreach ($query as $field => $field_value) {
            $deleteQuery->condition($field, $field_value);
          }
          $deleteQuery->execute();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEncryptionKey() {
    $encryption_profile_id = Drupal::config('field_encrypt.settings')
      ->get('encryption_profile');
    $encryption_profile = Drupal::service('encrypt.encryption_profile.manager')
      ->getEncryptionProfile($encryption_profile_id);
    return $encryption_profile->getEncryptionKey()->getKeyValue();
  }

  /**
   * Create blind indexes for value.
   *
   * @param $engine
   * @param $value
   */
  private function createBlindIndexesForValueReact($engine, $value, $entity, $fieldName, $delta, $property) {
    $promises = [];
    $loop = Loop::get();
    $length = 1;
    $valueLength = mb_strlen($value);
    $timer = $loop->addPeriodicTimer(0.0000001, function (\React\EventLoop\TimerInterface $timer)
    use (&$length, $engine, $value, $valueLength, &$promises, $loop, $entity, $fieldName, $delta, $property) {
      $tmpLength = $length;
      $length++;
      if ($tmpLength > $valueLength) {
        $loop->cancelTimer($timer);
        all($promises)->then(function ($indexes) use ($entity, $fieldName, $delta, $property) {
          $this->removeIndexFromDB($entity, $fieldName, $delta, $property);
          foreach ($indexes as $items) {
            $this->insertIndexIntoDB($entity, $fieldName, $delta, $property, $items);
          }
        });
      }
      else {
        $indexArr = [];
        for ($position = 0; $position <= $valueLength - $tmpLength; $position++) {
          $prepareBlindIndex = new EncryptedField($engine);
          $prepareBlindIndex->addBlindIndex(
            new BlindIndex(
              "encrypt_searchable",
              [new SearchLikeTransformation($position, $tmpLength)],
              128,
              TRUE
            )
          );
          $indexArr[] = "{$position}-{$tmpLength}|" . $prepareBlindIndex->getBlindIndex($value, 'encrypt_searchable');
        }
        $deferred = new Deferred();
        $deferred->resolve($indexArr);
        $promises[] = $deferred->promise();
      }
    });
  }

  /**
   * Remove old blind indexes.
   *
   * @param $entity
   * @param $blindIndexField
   * @param $delta
   * @param $property
   */
  private function removeIndexFromDB($entity, $blindIndexField, $delta, $property) {
    $query = [
      'langcode' => $entity->language()->getId() ?? Drupal::languageManager()
          ->getDefaultLanguage()
          ->getId(),
      'status' => TRUE,
      'entity_id' => $entity->id(),
      'entity_bundle' => $entity->bundle(),
      'entity_field_property' => $property,
      'entity_field_delta' => $delta,
    ];

    $tableName = "blind_index__{$entity->getEntityTypeId()}__{$blindIndexField}";

    $deleteQuery = $this->databaseConnection->delete($tableName);
    foreach ($query as $field => $field_value) {
      $deleteQuery->condition($field, $field_value);
    }
    $deleteQuery->execute();
  }

  /**
   * Insert new blind indexes.
   *
   * @param $entity
   * @param $blindIndexField
   * @param $delta
   * @param $indexes
   */
  private function insertIndexIntoDB($entity, $blindIndexField, $delta, $property, $indexes) {
    $query = [
      'langcode' => $entity->language()->getId() ?? Drupal::languageManager()
          ->getDefaultLanguage()
          ->getId(),
      'status' => TRUE,
      'entity_id' => $entity->id(),
      'entity_bundle' => $entity->bundle(),
      'entity_field_property' => $property,
      'entity_field_delta' => $delta,
    ];

    $tableName = "blind_index__{$entity->getEntityTypeId()}__{$blindIndexField}";

    $insertQuery = $this->databaseConnection->insert($tableName)
      ->fields(array_merge(array_keys($query), [
        'name',
        'index_value',
      ]));

    foreach ($indexes as $index) {
      $index = explode('|', $index);
      $insertQuery->values(array_merge($query, [
        'name' => "$property--[{$index[0]}]",
        'index_value' => $index[1],
      ]));
    }
    $insertQuery->execute();
  }

}

<?php

namespace Drupal\field_encrypt_searchable\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BatchBlindIndexForm.
 *
 * @package Drupal\field_encrypt_searchable\Form
 */
class BatchBlindIndexForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_encrypt_searchable_blind_index_batch';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['field_encrypt_searchable_blind_index_batch.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::service('entity_field.manager');

    $config = $this->config('field_encrypt_searchable_blind_index_batch.settings');
    $blind_index_bundle = [];
    $field_map = $entity_field_manager->getFieldMap();
    foreach ($field_map as $entity_type => $fields) {
      $field_storage_definitions = $entity_field_manager->getFieldStorageDefinitions($entity_type);
      foreach ($field_storage_definitions as $fieldId => $field_storage_definition) {
        if (!is_callable([$field_storage_definition, 'getThirdPartySetting'])) {
          continue;
        }
        $has_blind_index = $field_storage_definition->getThirdPartySetting('field_encrypt', 'encrypt', FALSE)
          && $field_storage_definition->getThirdPartySetting('field_encrypt', 'blind_index', FALSE);
        $blind_index_properties = $field_storage_definition->getThirdPartySetting('field_encrypt', 'properties', []);
        if ($has_blind_index && !empty($blind_index_properties)) {
          if (isset($blind_index_bundle[$entity_type])) {
            $blind_index_bundle[$entity_type] += $fields[$fieldId]['bundles'];
          }
          else {
            $blind_index_bundle[$entity_type] = $fields[$fieldId]['bundles'];
          }
        }
      }
    }

    $entity_type_definitions = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($blind_index_bundle as $type => $bundles) {
      $default_bundles = \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo($type);
      $bundle_options = [];
      foreach ($bundles as $bundle) {
        if (isset($default_bundles[$bundle])) {
          $bundle_options[$bundle] = $default_bundles[$bundle]['label'];
        }
      }
      $form["bundles_wrapper_$type"] = [
        '#type' => 'details',
        '#title' => $entity_type_definitions[$type]->getLabel(),
        '#open' => FALSE,
      ];
      if (!empty($config->get("bundles_$type"))) {
        foreach ($config->get("bundles_$type") as $value) {
          if ($value !== 0) {
            $form["bundles_wrapper_$type"]['#open'] = TRUE;
            break;
          }
        }
      }

      $form["bundles_wrapper_$type"]["bundles_$type"] = [
        '#type' => 'checkboxes',
        '#options' => $bundle_options,
        '#default_value' => $config->get("bundles_$type"),
      ];
    }

    $form['batch_size'] = [
      '#type' => 'number',
      '#max' => 100,
      '#min' => 1,
      '#default_value' => $config->get('batch_size') ? $config->get('batch_size') : 20,
      '#required' => TRUE,
      '#title' => t('Batch size'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Blind index'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('field_encrypt_searchable_blind_index_batch.settings');
    foreach ($form_state->getValues() as $key => $value) {
      if (substr($key, 0, 8) === 'bundles_') {
        $config->set($key, $value);
      }
    }
    $batchSize = $form_state->getValue('batch_size');
    $config->set('batch_size', $batchSize);
    $config->save();

    $totalCount = 0;
    $operations = [];

    foreach ($form_state->getValues() as $key => $value) {
      if (substr($key, 0, 8) === 'bundles_') {
        $entityTypeId = substr($key, 8);

        $entityDefinitionManager = Drupal::entityDefinitionUpdateManager();
        $entityTypeLoad = $entityDefinitionManager->getEntityType($entityTypeId);
        $entityKeys = $entityTypeLoad->getKeys();
        $bundleKey = $entityKeys['bundle'];
        $idKey = $entityKeys['id'];

        foreach ($value as $bundle) {
          $entityCountQuery = Drupal::entityQuery($entityTypeId);
          if (!empty($idKey)) {
            $entityCountQuery->condition($idKey, 0, '<>');
          }
          if (!empty($bundleKey)) {
            $entityCountQuery->condition($bundleKey, $bundle);
          }
          $entityCount = $entityCountQuery->count()->execute();
          if ($entityCount > 0) {
            $totalCount += $entityCount;
            for ($i = 0; $i <= ($entityCount / $batchSize); $i++) {
              $operations[] = [
                '\Drupal\field_encrypt_searchable\Form\BatchBlindIndexForm::processItem',
                [
                  $i,
                  $batchSize,
                  $entityTypeId,
                  $bundle,
                ],
              ];
            }
          }
        }
      }
    }

    if (!empty($operations)) {
      $totalOperation = count($operations);
      foreach ($operations as $operationId => &$operation) {
        $operation[1][] = $operationId + 1;
        $operation[1][] = $totalOperation;
      }
      $batch = [
        'title' => "Blind Index for Entity",
        'operations' => $operations,
        'init_message' => \Drupal::translation()->formatPlural($totalCount, 'Processing 1 record.', 'Processing @count records.'),
        'finished' => '\Drupal\field_encrypt_searchable\Form\BatchBlindIndexForm::processFinish',
      ];

      batch_set($batch);
    }
  }

  /**
   * processItem.
   */
  public static function processItem($step, $batchSize, $entityTypeId, $bundle, $operationId, $totalOperation, &$context)
  {
    $fieldEncryptSearchableService = Drupal::service('field_encrypt_searchable.process_entities');
    $entityDefinitionManager = Drupal::entityDefinitionUpdateManager();
    $entityTypeLoad = $entityDefinitionManager->getEntityType($entityTypeId);
    $entityKeys = $entityTypeLoad->getKeys();
    $bundleKey = $entityKeys['bundle'];
    $idKey = $entityKeys['id'];

    $entityQuery = Drupal::entityQuery($entityTypeId);
    if (!empty($idKey)) {
      $entityQuery->condition($idKey, 0, '<>');
    }
    if (!empty($bundleKey)) {
      $entityQuery->condition($bundleKey, $bundle);
    }
    $entityIds = $entityQuery
      ->range($step * $batchSize, $batchSize)
      ->execute();

    $context['results'][] = count($entityIds);

    foreach ($entityIds as $id) {
      try {
        $entities = Drupal::entityTypeManager()
          ->getStorage($entityTypeId)
          ->loadByProperties([
            $entityKeys['id'] => $id,
          ]);
        if (!empty($entities)) {
          $entity = reset($entities);

          // Blind Index Entity.
          $fieldEncryptSearchableService->reProcessBlindIndex($entity);
        } else {
          throw new \Exception("Not Found Entity!");
        }
      } catch (\Exception $ex) {
        Drupal::logger('field_encrypt_searchable_blind_index')
          ->error("Cannot blind index entity has id=$id (Entity Type: $entityTypeId, Bundle: $bundle, Error: " . $ex->getMessage());
      }
    }
    if ($operationId % 10 == 0 || $operationId == $totalOperation) {
      Drupal::logger('field_encrypt_searchable_blind_index')
        ->info("Processed page {$operationId}/$totalOperation");
    }
  }


  /**
   * processFinish.
   */
  public static function processFinish($success, array $results, array $operations)
  {
    $messenger = \Drupal::messenger();
    if ($success) {
      // Here we could do something meaningful with the results.
      // We just display the number of nodes we processed...
      $count = array_sum($results);
      $messenger->addMessage(
        \Drupal::translation()->formatPlural(
          $count,
          '1 result processed.',
          '@count results processed.'
        )
      );
    } else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $messenger->addMessage(
        t(
          'An error occurred while processing @operation with arguments : @args',
          [
            '@operation' => $error_operation[0],
            '@args' => print_r($error_operation[0], TRUE),
          ]
        )
      );
    }
  }

}

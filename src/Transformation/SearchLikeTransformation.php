<?php

namespace Drupal\field_encrypt_searchable\Transformation;

use ParagonIE\ConstantTime\Binary;
use ParagonIE\CipherSweet\Contract\TransformationInterface;

/**
 * Class SearchLikeTransformation.
 * @package ParagonIE\CipherSweet\Transformation
 */
class SearchLikeTransformation implements TransformationInterface
{

  private $position;

  private $length;

  public function __construct($position, $length)
  {
    $this->position = $position;
    $this->length = $length;
  }

  /**
   * Return substring from $position with length $length.
   *
   * @param string $input
   * @return string
   */
  public function __invoke($input)
  {
    return Binary::safeSubstr(mb_strtolower($input), $this->position, $this->length);
  }
}

<?php

namespace Drupal\field_encrypt_searchable;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

class BlindIndexEntityStorageSchema extends SqlContentEntityStorageSchema {

  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    $schema['blind_index_entity']['indexes'] += array(
      'blind_index_delete' => array('langcode', 'status', 'entity_id', 'entity_type_id', 'entity_bundle', 'entity_field'),
    );

    return $schema;
  }


}

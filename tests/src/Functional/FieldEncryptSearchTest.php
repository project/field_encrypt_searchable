<?php

namespace Drupal\Tests\field_encrypt_searchable\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\field_encrypt\Functional\BaseFieldTest;
use Drupal\views\Views;

/**
 * Test filter encrypted_field_filter can search encrypted field data with test plan:
 * 1. Create key encrypted.
 * 2. Create profile encrypted.
 * 3. Set profile for field encrypted.
 * 4. Create content type Basic Page (page).
 * 5. Create field phone encrypted with 3 third party ("encrypt","properties","blind_index").
 * 6. Enable created phone field in content type page form mode.
 * 7. Enable created phone field in content type page view mode.
 * 8. Remove basic filter of "content" view and replace with "encrypted_field_filter".
 * 9. Create 2 page node content and check data display at "node/{node_id}" and "admin/content".
 * 10. Search field phone with one of two in sample data and expected display one of them in "admin/content".
 *
 * @group field_encrypt_searchable
 */
class FieldEncryptSearchTest extends BaseFieldTest {

  /**
   * {@inheritdoc}
   *
   */
  public static $modules = [
    'node',
    'field',
    'field_ui',
    'text',
    'locale',
    'content_translation',
    'key',
    'encrypt',
    'encrypt_test',
    'field_encrypt',
    'field_encrypt_test',
    'field_encrypt_searchable',
    'real_aes',
    'views_ui',
  ];

  /**
   * {@inheritdoc}
   *
   * @TODO: Simplify setUp() by extending EncryptTestBase when
   *   https://www.drupal.org/node/2692387 lands.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->settingFieldEncrypt();
    $this->createPhoneFieldEncrypted();
    $this->enabledPhoneFieldFormMode();
    $this->enabledPhoneFieldViewMode();
    $this->addFieldEncryptedFilterToPageContent();
  }


  /**
   * Tests content search form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testContentSearchForm() {
    $this->setFieldStorageSettings(TRUE);
    $args_data = [
      'Field Encrypt Search Test 01' => '0123456789',
      'Field Encrypt Search Test 02' => '3589652233',
    ];
    foreach ($args_data as $title => $phone) {
      $test_node = $this->drupalCreateNode([
        'type' => 'page',
        'title' => $title,
        'field_phone' => $phone,
      ]);
      $this->assertEquals($test_node->get('title')->value, $title);
      $this->assertEquals($test_node->get('field_phone')->value, $phone);
      $this->drupalGet('/node/' . $test_node->id());
      $this->assertSession()->pageTextContains($title);
      $this->assertSession()->pageTextContains($phone);
      $this->drupalGet('/admin/content');
    }

    $this->drupalGet('/admin/content?encrypted_field_filter=0123456789');
    $this->assertSession()->pageTextContains('Field Encrypt Search Test 01');
    $this->assertSession()->pageTextNotContains('Field Encrypt Search Test 02');

    $this->drupalGet('/admin/content?encrypted_field_filter=3589652233');
    $this->assertSession()->pageTextContains('Field Encrypt Search Test 02');
    $this->assertSession()->pageTextNotContains('Field Encrypt Search Test 01');


  }

  /**
   * Create field phone encrypted for test.
   */
  protected function createPhoneFieldEncrypted() {
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_phone',
      'entity_type' => 'node',
      'type' => 'string',
    ]);

    $field_storage->setThirdPartySetting('field_encrypt', 'encrypt', TRUE);
    $field_storage->setThirdPartySetting('field_encrypt', 'blind_index', TRUE);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'page',
    ]);
    $field->save();

    $this->drupalGet('/admin/structure/types/manage/page/fields/node.page.field_phone/storage');
    $this->assertSession()->pageTextContains('Encrypt field');
    $this->assertSession()->pageTextContains('Blind index');
    $this->assertSession()->pageTextContains('Text value');
    $this->assertSession()->checkboxChecked('edit-field-encrypt-encrypt');
    $this->assertSession()->checkboxChecked('edit-field-encrypt-properties-value');
    $this->assertSession()->checkboxChecked('edit-field-encrypt-blind-index');
  }

  /**
   * Choose profile for field encrypt.
   */
  protected function settingFieldEncrypt() {
    $this->drupalGet('/admin/config/system/field-encrypt');
    $field = [];
    $field['encryption_profile'] = 'encryption_profile_2';
    $this->submitForm($field, 'Save configuration');
  }

  /**
   * Enable display for page form mode.
   */
  protected function enabledPhoneFieldFormMode() {
    \Drupal::entityTypeManager()
      ->getStorage('entity_form_display')
      ->load('node.page.default')
      ->setComponent('field_phone', [])->save();
  }

  /**
   * Enable display for page view mode.
   */
  protected function enabledPhoneFieldViewMode() {
    \Drupal::entityTypeManager()
      ->getStorage('entity_view_display')
      ->load('node.page.default')
      ->setComponent('field_phone', [])->save();
  }

  /**
   * Add filter to page content to search.
   */
  protected function addFieldEncryptedFilterToPageContent() {
    $view = Views::getView('content');
    $view->initDisplay();

    $filters['encrypted_field_filter'] = [
      'id' => 'encrypted_field_filter',
      'table' => 'views',
      'field' => 'encrypted_field_filter',
      'relationship' => 'none',
      'group_type' => 'group',
      'admin_label' => '',
      'operator' => 'contains',
      'value' => '',
      'group' => 1,
      'exposed' => TRUE,
      'expose' => [
        'operator_id' => 'encrypted_field_filter_op',
        'label' => 'Phone',
        'description' => '',
        'use_operator' => FALSE,
        'operator' => 'encrypted_field_filter_op',
        'operator_limit_selection' => FALSE,
        'operator_list' => [],
        'identifier' => 'encrypted_field_filter',
        'required' => FALSE,
        'remember' => FALSE,
        'multiple' => FALSE,
      ],
    ];

    $view->displayHandlers->get('default')->overrideOption('filters', $filters);
    $view->setDisplay();
    $view->save();

    $this->drupalGet('/admin/structure/views/view/content');
    $this->assertSession()->pageTextContains('Global: Encrypted field filter (exposed)');
  }

}

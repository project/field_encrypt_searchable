<?php

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field_encrypt_searchable\FieldEncryptSearchableProcessEntitiesInterface;

/**
 * Implements hook_field_views_data_alter().
 */
function field_encrypt_searchable_field_views_data_alter(array &$data, \Drupal\field\FieldStorageConfigInterface $field_storage)
{
  $data['views']['encrypted_field_filter'] = [
    'title' => t('Encrypted field filter'),
    'help' => t('Provides a custom filter for encrypted fields.'),
    'filter' => [
      'id' => 'encrypted_field_filter',
    ],
  ];
}

/**
 * Implements hook_entity_insert().
 */
function field_encrypt_searchable_entity_insert(Drupal\Core\Entity\EntityInterface $entity)
{
  if ($entity instanceof ContentEntityInterface) {
    /** @var FieldEncryptSearchableProcessEntitiesInterface $feProcessService */
    $feProcessService = Drupal::service('field_encrypt_searchable.process_entities');
    $feProcessService->processBlindIndex($entity);
  }
}

/**
 * Implements hook_entity_update().
 */
function field_encrypt_searchable_entity_update(Drupal\Core\Entity\EntityInterface $entity)
{
  if ($entity instanceof ContentEntityInterface) {
    /** @var FieldEncryptSearchableProcessEntitiesInterface $feProcessService */
    $feProcessService = Drupal::service('field_encrypt_searchable.process_entities');
    $feProcessService->processBlindIndex($entity);
  }
  elseif ($entity instanceof FieldStorageConfig) {
    $databaseSchema = Drupal::database()->schema();
    $entityTypeId = $entity->getTargetEntityTypeId();
    $entityFieldId = $entity->getName();
    $tableName = "blind_index__{$entityTypeId}__{$entityFieldId}";

    if (!empty($entity->field_encrypt['encrypt'])
      && !empty($entity->field_encrypt['blind_index'])
      && $entity->field_encrypt['encrypt'] == 1
      && $entity->field_encrypt['blind_index'] == 1) {
      if (!$databaseSchema->tableExists($tableName)) {
        $schemaDefinition = blind_index_schema_definition();
        $databaseSchema->createTable($tableName, $schemaDefinition);
      }
    }
    else {
      if ($databaseSchema->tableExists($tableName)) {
        $databaseSchema->dropTable($tableName);
      }
    }
  }
}

/**
 * Implements hook_entity_delete().
 */
function field_encrypt_searchable_entity_delete(Drupal\Core\Entity\EntityInterface $entity)
{
  if ($entity instanceof ContentEntityInterface) {
    /** @var FieldEncryptSearchableProcessEntitiesInterface $feProcessService */
    $feProcessService = Drupal::service('field_encrypt_searchable.process_entities');
    $feProcessService->deleteBlindIndex($entity);
  }
  elseif ($entity instanceof FieldStorageConfig) {
    $databaseSchema = Drupal::database()->schema();
    $entityTypeId = $entity->getTargetEntityTypeId();
    $entityFieldId = $entity->getName();
    $tableName = "blind_index__{$entityTypeId}__{$entityFieldId}";

    if ($databaseSchema->tableExists($tableName)) {
      $databaseSchema->dropTable($tableName);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function field_encrypt_searchable_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id)
{
  if (in_array($form_id, ['field_storage_add_form', 'field_storage_config_edit_form'])) {
    // Check permissions.
    $user = \Drupal::currentUser();
    if ($user->hasPermission('administer field encryption')) {
      /* @var $field \Drupal\field\Entity\FieldStorageConfig */
      $field = $form_state->getFormObject()->getEntity();

      $form['field_encrypt']['field_encrypt']['blind_index'] = [
        '#type' => 'checkbox',
        '#title' => t('Blind index'),
        '#description' => t('Blind index the field for searching.'),
        '#default_value' => $field->getThirdPartySetting('field_encrypt', 'blind_index', false),
        '#states' => [
          'visible' => [
            ':input[name="field_encrypt[encrypt]"]' => array('checked' => TRUE),
          ],
        ],
      ];

      // We add functions to process the form when it is saved.
      $form['#entity_builders'][] = 'field_encrypt_searchable_form_field_add_form_builder';
    }
  }
}

/**
 * Update the field storage configuration to set the encryption state.
 *
 * @param string $entity_type
 *   The entity type.
 * @param \Drupal\field\Entity\FieldStorageConfig $field_storage_config
 *   The field storage config entity.
 * @param array $form
 *   The complete form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The current state of the form.
 */
function field_encrypt_searchable_form_field_add_form_builder($entity_type, \Drupal\field\Entity\FieldStorageConfig $field_storage_config, &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  $field_encryption_settings = $form_state->getValue('field_encrypt');
  $field_encryption_settings['encrypt'] = (bool) $field_encryption_settings['encrypt'];

  // If the form has the value, we set it.
  if ($field_encryption_settings['encrypt']) {
    foreach ($field_encryption_settings as $settings_key => $settings_value) {
      $field_storage_config->setThirdPartySetting('field_encrypt', $settings_key, $settings_value);
    }
  }
  else {
    // If there is no value, remove third party settings.
    $field_storage_config->unsetThirdPartySetting('field_encrypt', 'encrypt');
    $field_storage_config->unsetThirdPartySetting('field_encrypt', 'properties');
    $field_storage_config->unsetThirdPartySetting('field_encrypt', 'blind_index');
  }
}

/**
 * Schema definition for Blind index.
 */
function blind_index_schema_definition() {
  return [
    'fields' => [
      'id' => [
        'type' => 'serial',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'langcode' => [
        'type' => 'varchar_ascii',
        'length' => 12,
        'not null' => TRUE,
      ],
      'status' => [
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
      ],
      'name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'entity_id' => [
        'type' => 'int',
        'not null' => TRUE,
      ],
      'entity_bundle' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
      ],
      'entity_field_property' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
      ],
      'entity_field_delta' => [
        'type' => 'int',
        'not null' => TRUE,
      ],
      'index_value' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'blind_index_delete' => ['langcode', 'status', 'entity_id', 'entity_bundle'],
    ],
  ];
}
